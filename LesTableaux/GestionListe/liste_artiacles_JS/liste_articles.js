// Programme qui calcul le prix de chaque articles ainsi que la totalité 
// Thomas ALEXANDRE 29/04/2021

let listeArticles = [['Jus orange 1L',2 , 1.35],["Yaourt nature 4X",1,1.60],["Pain de mie 500g",1,0.9],["Barquette Jambon blanc 4xT",1,2.75],["Salade Laitue",1,0.8],["Spaghettis 500g",2,0.95]]; // Liste d'article
let prix = 0; // prix de l'article en fonction de la quantité acheté 
let total = 0; // prix total
let cpt = 0;    // Nombre d'article

for(let liste of listeArticles) {
    prix = liste[1] * liste[2]; // Calcul du prix en fonction de la quantité 
    console.log(liste[0] + "\n"+ liste[1] + " X " + liste[2].toFixed(2)+" EUR\t\t\t"+ prix.toFixed(2) +" EUR"); //  Affichage de l'article du prix et de la quatité
    total += prix;              // Calcul du prix total 
    cpt+= liste[1];             // Nombre d'article
    prix = 0;                   // Remise à 0 du prix
}

console.log("Nombre d'articles achetés: "+cpt);         // Affichage du total d'article 
console.log("MONTANT: "+total.toFixed(2)+" EUR");       // Affichage du montant total
