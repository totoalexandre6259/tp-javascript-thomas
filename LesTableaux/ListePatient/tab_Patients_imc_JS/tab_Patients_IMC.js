// Programme qui permet de trier les patients dans un tableau
// Thomas ALEXANDRE 11/05/2021

function Patient(prmNom,prmPrenom,prmAge,prmSexe,prmTaille,prmPoids) {
    this.nom = prmNom ;  // Nom de la personne 
    this.prenom = prmPrenom ; // Prenom de la personne 
    this.age = prmAge ; // Age de la personne 
    this.poids = prmPoids; // Poids de la personne 
    this.taille = prmTaille; // Taille de la personne 
    this.sexe = prmSexe // Sexe de la personne 
    this.imc = this.poids / ((this.taille / 100) * (this.taille / 100));    // Calcul de l'imc de la personne 
}

function afficher_ListePatients(prmTabPatients){
    let message = ""    // message de retour
    for(let patients of prmTabPatients){
        message = message + patients.nom + " " + patients.prenom+"\n";  // Affichage du nom et prenom des patiens
    }
    return console.log(message);   // message de retour 

}
function afficher_ListePatients_Par_Sexe(prmTabPatients,prmSexe){
    let message = "" // message de retour
        
        for(let patients of prmTabPatients){
            if(prmSexe == patients.sexe){ // Triage en fonction du sexe
            message = message + patients.nom + " " + patients.prenom+"\n"; 
            }
        }  

    return console.log(message); // message de retour

}

function afficher_ListePatients_Par_Corpulence(prmTabPatients,prmCorpulence) {
    console.log("Liste des patients en état de "+ prmCorpulence+" :");
    let message ="";    // message de retour
    let imc;            // Valeur de l'imc
    let corpulence;     // Corpulence du patient 
    for(let patients of prmTabPatients){

        imc = patients.imc; // imc prends la valeur de patients.imc

        if (patients.sexe == 'Masculin') {
            imc = imc + 2; // ajout de 2 à l'imc masculin 
        }

        // On cherche la corpulence du patient 
        if (imc < 16.5) corpulence = "Dénutrition"; 
        if ((imc>16.5) && (imc < 18.5)) corpulence = "Maigreur"; 
        if ((imc>18.5) && (imc < 25)) corpulence = "corpulence normale"; 
        if ((imc>25) && (imc < 30)) corpulence = "Surpoids"; 
        if ((imc>30) && (imc < 35)) corpulence = "Obésité modérée"; 
        if ((imc>35) && (imc < 40)) corpulence = "Obésité sévère"; 
        if (imc > 40) corpulence = "Obésité morbide"; 

        if(corpulence == prmCorpulence){
            message = message + patients.prenom +" " + patients.nom+ " a un IMC = "+ patients.imc.toFixed(2)+"\n"; // message de retour
        }

    }  

return console.log(message); // message de retour

} 

function afficher_DescriptionPatient(prmTabPatients, prmNom) {
    let message="Ce nom n'existe pas"; // message de retour
    let imc;                            // valeur de l'imc
    let corpulence;                     // Corpulence du passion
    // IHM
    console.log("Description du patient " + prmNom);

    for(let patients of prmTabPatients){
        if (prmNom == patients.nom) {
         imc = patients.imc; // imc prends la valeur de patients.imc

         if (patients.sexe == 'Masculin') {
            imc = imc + 2;   // imc + 2 si c'est un Homme
         }
         // on cherche la corpulence du patient 
         if (imc < 16.5) corpulence = "Dénutrition"; 
         if ((imc>16.5) && (imc < 18.5)) corpulence = "Maigreur"; 
         if ((imc>18.5) && (imc < 25)) corpulence = "corpulence normale"; 
         if ((imc>25) && (imc < 30)) corpulence = "Surpoids"; 
         if ((imc>30) && (imc < 35)) corpulence = "Obésité modérée"; 
         if ((imc>35) && (imc < 40)) corpulence = "Obésité sévère"; 
         if (imc > 40) corpulence = "Obésité morbide"; 

            // // message de retour
            message = "Le patient " + patients.prenom +" "+patients.nom + " est agé de "+ patients.age+" ans. Il mesure "+ patients.taille+"m et pèse "+patients.poids+"kg.\n"
            +"L'IMC de ce patient est de : "+patients.imc.toFixed(2)+"\n"
            +"Il est en situation de "+ corpulence; 
         
        }
        

    }
    return console.log(message);    // message de retour
}
// Liste des patients 
let objPatient0 = new Patient('Dupond', 'Jean', 30, 'Masculin', 180, 85);
let objPatient1 = new Patient('Martin', 'Eric', 42, 'Masculin', 165, 90);
let objPatient2 = new Patient('Moulin', 'Isabelle', 46, 'Féminin', 158, 74);
let objPatient3 = new Patient('Verwaerde', 'Paul', 55, 'Masculin', 177, 66);
let objPatient4 = new Patient('Durand', 'Dominique', 60, 'Féminin', 163, 54);
let objPatient5 = new Patient('Lejeune', 'Bernard', 63, 'Masculin', 158, 78);
let objPatient6 = new Patient('Chevalier', 'Louise', 35, 'Féminin', 170, 82);

// Déclaration du tableau
let tabPatients = [objPatient0, objPatient1, objPatient2, objPatient3, objPatient4, objPatient5, objPatient6];

// Appel des fonctions 
afficher_ListePatients(tabPatients);
afficher_ListePatients_Par_Sexe(tabPatients,"Masculin");
afficher_ListePatients_Par_Corpulence(tabPatients,"Surpoids");
afficher_DescriptionPatient(tabPatients, "Dupond");
afficher_DescriptionPatient(tabPatients, "Brassart");













