// Programme qui permet de calculer la moyenne d'un tableau
// Thomas ALEXANDRE 29/04/2021

let listeValeur = [10,25,41,5,9,11];        // Chiffre du tableau
let moyenne = 0;                            // Moyenne du tableau
let constante = 6;                          // Constante

console.log("Liste des valeurs :");         // IHM

for(let i = 0; i<constante;i++){    

console.log(listeValeur[i]);    // Affichage des valeurs du tableau
moyenne = moyenne + listeValeur[i]; // Calcul de la somme total

}
console.log("Somme des valeurs : "+ moyenne); // IHM
moyenne = moyenne / constante;  // Calcul de la moyenne 
console.log("Moyenne des valeurs :"+moyenne.toFixed(2)); // IHM

