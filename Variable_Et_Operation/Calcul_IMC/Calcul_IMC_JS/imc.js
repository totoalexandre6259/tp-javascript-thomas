// Déclaration des variables
let poids = 100;                            // poids en kg
let taille = 160;                           // taille en cm
let IMC = undefined;                        // Résultat de l'imc

IMC = poids / ((taille * taille) * 10e-5);

// IHM
console.log("Calcul de l'IMC");
console.log("taille :" + taille + "cm");    // Affichage de la variable taille 
console.log("poids : " + poids + "kg");     // Affichage de la variable poids
console.log("IMC =" + IMC.toFixed(1));      // Affichage de la variable IMC
