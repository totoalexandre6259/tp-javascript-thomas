
# TP JavaScript Thomas 

Mes TP en javaScript

> * Auteur : Thomas ALEXANDRE
> * Date de publication : 06/04/2021



## Sommaire

  - 1. Introduction
        - [HelloWorld](Introduction/HelloWorld_JS/index.html)
  - 2. Variable et Calcul
        - [CalculSurface](Variable_Et_Operation/Surface/index.html)
        - [CalculIMC](Variable_Et_Operation/Calcul_IMC/index.html)
        - [ConversionFahrenheit](Variable_Et_Operation/Celcius_Fahrenheit/index.html)
  - 3. Structure de controle
        - [InterpretationIMC](Structure_De_Controle/interpertation_IMC/index.html)
        - [SuiteDeSyracus](Structure_De_Controle/Conjecture_de_Syracuse/index.html)
        - [CalculFactorielle](Structure_De_Controle/CalculFactorielle/index.html)
        - [ConversionEuroDollar](Structure_De_Controle/ConversionEuro_Dollars/index.html)
        - [NombreTriples](Structure_De_Controle/NombresTriples/index.html)
        - [TableDeMultiplication](Structure_De_Controle/TableDeMultiplication/index.html)
  - 4. Les Fonctions
        - [InterpretationIMC_AvecFonction](LesFonctions/InterpreterIMC/index.html)
        - [CalculTempsTrajet](LesFonctions/CalculTempsTrajet/index.html)
        - [Multiplede3](LesFonctions/MultipleDe3/index.html)
        - [IMC_AvecFonctionIntegrer](LesFonctions/InterpreterIMCintergrer/index.html)
  - 5. Les Objets
        - [IMC_Avec_ObjetV1](Les_Objets/CalculIMC/index.html)
        - [IMC_Avec_Objet+FonctionIntegrerV2](Les_Objets/CalculIMC_FonctionInterne/index.html)
        - [IMC_AvecConstructeurV3](Les_Objets/CalculIMC_AvecConstructeur/index.html)
        - [IMC_AvecConstructeurOptimiséV4](Les_Objets/CalculIMC_AvecConstructeur%20Optimiser/index.html)
        - [Implémentation_Héritage](Les_Objets/Heritage_Proto/index.html)
        - [PersonnageDeJeu](Les_Objets/PersonnageDeJeu/index.html)
  - 6. Les Tableaux
        - [CalculMoyenne](LesTableaux/CalculMoyenne/index.html)
        - [GestionListeD'article](LesTableaux/GestionListe/index.html)
        - [ListePatients](LesTableaux/ListePatient/index.html)