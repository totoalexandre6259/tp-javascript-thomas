// Fichier fibonacci.js
// programme qui affiche la suite de fibonacci 
// Thomas Alexandre 08/04/2021


let n = 0; // Nombre de la suite
let n2 = 1; // Nombre de la suite 2
let i = 17; // limite
let resultat; // resultat de n + n2
let message = "Suite de Fibonacci : " + n + " " + n2; // initialisation du premier message
for (let j = 0; j < i; j++) {

  resultat = n + n2; // calcul
  n = n2; // n prends la valeur n2
  n2 = resultat; // n2 prends la valeur de resultat

  message += " " + resultat; // mise à jour du message
}

console.log(message); // affichage du message
