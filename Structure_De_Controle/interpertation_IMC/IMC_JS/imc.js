// Déclaration des variables
let poids = 90;                            // poids en kg
let taille = 170;                           // taille en cm
let IMC = undefined;                        // Résultat de l'imc

IMC = poids / ((taille * taille) * 10e-5);

// IHM
console.log("Calcul de l'IMC");
console.log("taille :" + taille + "cm");    // Affichage de la variable taille 
console.log("poids : " + poids + "kg");     // Affichage de la variable poids
console.log("IMC =" + IMC.toFixed(1));      // Affichage de la variable IMC

if (IMC < 16.5) {
    console.log("Vous etes en Dénutrition");
}
if ((IMC > 16.5) && (IMC < 18.5)) {
    console.log("Vous etes en etat de maigreur");
}
if ((IMC > 18.5) && (IMC < 25)) {
    console.log("Vous etes en etat de Corpulence normale");
}
if ((IMC > 25) && (IMC < 30)) {
    console.log("Vous etes en etat de surpoids");
}
if ((IMC > 30) && (IMC < 35)) {
    console.log("Vous etes en etat d'obesite modérée");
}
if ((IMC > 35) && (IMC < 40)) {
    console.log("Vous etes en etat d'obesite severe'");
}
if (IMC > 40) {
    console.log("Vous etes en etat d'obesite morbide");
}