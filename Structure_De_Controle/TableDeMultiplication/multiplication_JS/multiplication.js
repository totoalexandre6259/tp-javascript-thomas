// Fichier multiplication.js
// Programme qui calcul la table de multiplication de 7 et met un asterix quand c'est un nombre multiple de 3
// Thomas Alexandre 08/04/2021

let resultat = 0; // resultat du calcul
let message = "La table de multiplication est : "; // initialisation du message

for (let i = 1; i <= 20; i++) {
  resultat = 0; // remise à 0 du resultat
  resultat = i * 7; // calcul

  if (resultat % 3 == 0) { // Si le reste de la division resultat/3 = 0 alors 

    message += "*"; // mise à jour du message 
  }
  message += resultat + " "; // mise à jour du message 
}
console.log(message); // Affichage du message 
