let n = 2;              // nombre de facteur
let resultat = 1;       // resultat de l'operation
let factorielle = 10;   //Nombre de factorielle
let message;            // message affiché
let i;                  // multiplicateur

if (factorielle > 1) {
    //Affichage de la premiere ligne 
    console.log("1! = 1");

    for (n = 2; n <= factorielle; n++) {

        message = n + "! = 1";  //initialisation du message
        resultat = 1;           // on vide la variable résultat

        for (i = 2; i <= n; i++) {

            message += "x" + i;     // Ajout au message 
            resultat = resultat * i; // Calcul du resultat

        }

        message += "= " + resultat; // Ajout au message 
        console.log(message); // Affichage du message 
    }
}
