// Fichier syracuse.js
// Programme qui permet d'afficher la suite de syracus
// Thomas Alexandre 07/04/2021

let syracus = 14;     // suite de syracus
let cpt = 0;          // temps de vol 
let max = syracus;    // initialisation du maximum   
let result = undefined; // resultat 
//IHM
console.log("Suite de Syracus pour " + syracus);

// Condition en cas d'erreur
if (syracus < 1) {
    console.log("Erreur Syracus est inférieur à 0")
}

else {
    while (syracus != 1) {

        if (syracus > max) { // Si syracus > max Alors
            max = syracus; // max = syracus
        }
        if (syracus % 2 == 0) { // Si syracus % 2 == 0 Alors
            syracus = syracus / 2;
            cpt++; // incrementation du compteur 
        }
        else { // Sinon Alors
            syracus = (syracus * 3) + 1; // Syracus= syracus *3 +1
            cpt++; // incrementation du compteur 
        }
        result = result + "-" + syracus;
    }
}

//IHM
console.log(result);
console.log("Le temps de vol est de :" + cpt);
console.log("Altitude maximal = " + max);