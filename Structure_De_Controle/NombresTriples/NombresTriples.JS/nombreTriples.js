let nombre = 2;
let message;
let i;

console.log("Valeur de départ : 2");    // Affichage de la valeur de départ
message = "Valeurs de la suite: ";      // Mise à jour du message 

for (i = 1; i <= 12; i++) {

    nombre = nombre * 3;        // Calcul
    message += nombre + " ";    // Mise à jour du message 

}

console.log(message);           // Affichage du message
