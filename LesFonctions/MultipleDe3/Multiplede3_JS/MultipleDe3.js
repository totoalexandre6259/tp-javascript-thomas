// Fichier MultipleDe3.js
// programme qui permet de trouver les multiples de 3
// Thomas Alexandre 08/04/2021


let nombre = 0; // nombre testé
let multiple = 3; // multiple cherché
let limite = 20; // limite de la recherche 
let message = "Multiple de 3 : "; // message 

// IHM
console.log("Recherche du multiple de : " + multiple);
console.log("Valeur limite de la recherche : " + limite);

for (nombre = 0; nombre <= limite; nombre++) { // on teste tous les nombres allant de 0 à limite

  if (nombre % multiple == 0) { // Si le reste de la division nombre / multiple = 0 alors le nombre est un multiple 
    message += nombre + " "; // Mise à jour du message 
  }
}

console.log(message); // Affichage du message 
