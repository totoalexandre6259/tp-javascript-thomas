// Fichier tempsTrajet.js
// Programme qui permet de calculer le temps d'un trajet en heure minute seconde
// Thomas Alexandre 08/04/2021


let vitesse = 90; // vitesse en km/h
let distance = 500; // distance en km
let temps = undefined; // temps en seconde
let message; // message d'affichage

//IHM
console.log("Calcul du temps de parcours d'un trajet");
console.log("\t\tVitesse moyenne (en km/h) : \t \t" + vitesse);
console.log("\t\tDistance à parcourir (en km): \t\t" + distance);

//Fonction calculer le temps du parcour en seconde 
function calculerTempsParcoursSec(prmVitesse, prmDistance) {
  let resultat;     // resultat du temps 

  resultat = (prmDistance / prmVitesse) * 3600; // Calcul
  return resultat; // retourne la valeur de resultat 
}

temps = calculerTempsParcoursSec(vitesse, distance); // Appel de la fonction calculerTempsParcoursSec()

//IHM
console.log(
  "A " +
    vitesse +
    " km/h, " +
    "une distance de " +
    distance +
    " km" +
    " est parcourue en " +
    temps +
    " s"
);

// Fonction convertir en heure minute seconde
function convertir_h_min_sec(prmTemps) {
  let heure;    // heure
  let minute;   // minute
  let message;  // message

// Calcul heure minute seconde
  heure = Math.floor(prmTemps / 3600);
  prmTemps %= 3600;
  minute = Math.floor(prmTemps / 60);
  prmTemps %= 60;

  message = "Le trajet dure " + heure + "h , " + minute + " min et " + prmTemps + "sec";
  return message; // retourne le message
}

message = convertir_h_min_sec(temps); // Appel de la fonction convertir_h_min_sec()
console.log(message); // Affichage du message 


