// Déclaration des variables
let poids = 90; // poids en kg
let taille = 170; // taille en cm
let IMC = undefined; // Résultat de l'imc
let message; // message afficher

function calculerIMC(prmTaille, prmPoids) {
  let valIMC;
  valIMC = prmPoids / (prmTaille * prmTaille * 10e-5);
  return valIMC;
}
IMC = calculerIMC(taille, poids); // Appel de la fonction calculerIMC()

function interpreterIMC(prmIMC) {
  let interpretation = ""; // message d'interpretation

  if (prmIMC < 16.5) {
    // Si l'imc est inférieur à 16.5 Alors
    interpretation = "Vous etes en Dénutrition"; // MAJ de l'interpretation
  }
  if (prmIMC > 16.5 && prmIMC < 18.5) { 
      // Si l'imc est supérieur à 16.5  et inférieur à 18.5 Alors
    interpretation = "Vous etes en etat de maigreur"; // MAJ de l'interpretation
  }
  if (prmIMC > 18.5 && prmIMC < 25) {
            // Si l'imc est supérieur à 18.5 et inférieur à 25 Alors
    interpretation = "Vous etes en etat de Corpulence normale"; // MAJ de l'interpretation
  }
  if (prmIMC > 25 && prmIMC < 30) {
      // Si l'imc est supérieur à 25  et inférieur à 30 Alors      
    interpretation = "Vous etes en etat de surpoids"; // MAJ de l'interpretation
  }
  if (prmIMC > 30 && prmIMC < 35) {
      // Si l'imc est supérieur à 30 et inférieur à 35 Alors      
    interpretation = "Vous etes en etat d'obesite modérée"; // MAJ de l'interpretation
  }
  if (prmIMC > 35 && prmIMC < 40) {
      // Si l'imc est supérieur à 35  et inférieur à 40 Alors      
    interpretation = "Vous etes en etat d'obesite severe"; // MAJ de l'interpretation
  }
  if (prmIMC > 40) {
      // Si l'imc est supérieur à 40  
    interpretation = "Vous etes en etat d'obesite morbide"; // MAJ de l'interpretation
  }
  return interpretation; // retourne la valeur d'interpretation
}

message = interpreterIMC(IMC); // Appel de la fonction interpreterIMC()

// IHM
console.log("Calcul de l'IMC");
console.log("taille :" + taille + "cm");// Affichage de la variable taille
console.log("poids : " + poids + "kg"); // Affichage de la variable poids
console.log("IMC = " + IMC.toFixed(1)); // Affichage de la variable IMC
console.log(message);                   // Message d'interpretation 
