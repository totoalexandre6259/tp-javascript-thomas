// Déclaration des variables
let poids = 90; // poids en kg
let taille = 170; // taille en cm
let IMC = undefined; // Résultat de l'imc
let message; // message afficher

function decrire_corpulence(prmTaille, prmPoids) {
  let interpretation = ""; // message d'interpretation
  let valIMC;

  function calculerIMC() {
    valIMC = prmPoids / (prmTaille * prmTaille * 10e-5);
  }

  function interpreterIMC() {
  
    if (valIMC < 16.5) {
      // Si l'imc est inférieur à 16.5 Alors
      interpretation = "Vous etes en Dénutrition"; // MAJ de l'interpretation
    }
    if (valIMC > 16.5 && valIMC < 18.5) { 
        // Si l'imc est supérieur à 16.5  et inférieur à 18.5 Alors
      interpretation = "Vous etes en etat de maigreur"; // MAJ de l'interpretation
    }
    if (valIMC > 18.5 && valIMC < 25) {
              // Si l'imc est supérieur à 18.5 et inférieur à 25 Alors
      interpretation = "Vous etes en etat de Corpulence normale"; // MAJ de l'interpretation
    }
    if (valIMC > 25 && valIMC < 30) {
        // Si l'imc est supérieur à 25  et inférieur à 30 Alors      
      interpretation = "Vous etes en etat de surpoids"; // MAJ de l'interpretation
    }
    if (valIMC > 30 && valIMC < 35) {
        // Si l'imc est supérieur à 30 et inférieur à 35 Alors      
      interpretation = "Vous etes en etat d'obesite modérée"; // MAJ de l'interpretation
    }
    if (valIMC > 35 && valIMC < 40) {
        // Si l'imc est supérieur à 35  et inférieur à 40 Alors      
      interpretation = "Vous etes en etat d'obesite severe"; // MAJ de l'interpretation
    }
    if (valIMC > 40) {
        // Si l'imc est supérieur à 40  
      interpretation = "Vous etes en etat d'obesite morbide"; // MAJ de l'interpretation
    }
  }
  calculerIMC();
  interpreterIMC();
  return "Votre IMC est égal à " +valIMC.toFixed(2) +" " +interpretation; // retourne le message avec l'imc + l'interpretation
}

message = decrire_corpulence(taille, poids); // Appel de la fonction interpreterIMC()

// IHM
console.log("Calcul de l'IMC");
console.log("taille :" + taille + "cm");// Affichage de la variable taille
console.log("poids : " + poids + "kg"); // Affichage de la variable poids
console.log(message);                   // Message d'interpretation 
