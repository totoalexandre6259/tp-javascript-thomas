// Programme qui permet d'ajouter des professeurs ou des eleves et de faire une description simple ou avancé de cette dernière .
// Thomas ALEXANDRE 27/04/2021


function Personne(prmNom, prmPrenom, prmAge, prmSexe) {
    this.nom = prmNom;          // Nom de la personne 
    this.prenom = prmPrenom;   // Prenom de la personne 
    this.age = prmAge;   // age de la personne 
    this.sexe = prmSexe;   // sexe de la personne 
}

Personne.prototype.decrire = function() {
    let description;    // Message de description 
    description = "Cette personne s'appelle " + this.prenom + " " + this.nom + " elle est agée de " + this.age + " ans" ;
    return description; // retourne le message de description 
}

function Professeur(prmNom, prmPrenom, prmAge, prmSexe, prmMatiere) {
    Personne.call(this,prmNom, prmPrenom, prmAge, prmSexe); // Permet d'utiliser les propriété de la fonction personne
    this.matiere = prmMatiere;  // Matiere du professeur 

    Professeur.prototype.decrire = function(){
        let description; // Message de description 
        description = "Cette personne s'appelle "+ this.prenom + " " + this.nom + "elle est agée de " + this.age +" ans"
        return description; // Message de description 
    }
    Professeur.prototype.decrire_plus = function() {
        let description; // Message de description avancé
        let prefixe;    // prefixe de politesse
        if(this.sexe == 'M') {
            prefixe = 'Mr';
        } else {
            prefixe = 'Mme';
        }
        description = prefixe + " " + this.prenom + " " + this.nom + " est professeur de " + this.matiere;
        return description; // retourne le Message de description  avancé
    }
}

function Eleve(prmNom,prmPrenom,prmAge,prmSexe,prmClasse){
    Personne.call(this,prmNom,prmPrenom,prmAge,prmSexe) // Permet d'utiliser les propriété de la fonction personne
    this.classe = prmClasse;    // Classe de l'éleve

    Eleve.prototype.decrire = function(){
        let description; // Message de description
        description = "Cette personne s'appelle "+ this.prenom + " " + this.nom + "elle est agée de " + this.age +" ans"
        return description;  // retourne le Message de description
    }

    Eleve.prototype.decrire_plus = function() {
        let description; //Message de description  avancé
        let prefixe;  // prefixe de politesse
        if(this.sexe == 'M') {
            prefixe = 'Mr';
        } else {
            prefixe = 'Mme';
        }
        description = prefixe + " " + this.prenom + " " + this.nom + " est eleve en classe de  " + this.classe;
        return description; // retourne le Message de description  avancé
    }
}

// Création des objets
let objProfesseur1 = new Professeur('Dupond', 'Jean', 30, 'M', 'Mathématiques');
let objEleve1 = new Eleve('Dorian','Dutillieul',18,'M','SNIR1')

// Affichage
console.log(objProfesseur1.decrire());
console.log(objProfesseur1.decrire_plus());
console.log(objEleve1.decrire());
console.log(objEleve1.decrire_plus());

