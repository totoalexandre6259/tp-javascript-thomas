let objPersonne = {
    nom: "Dupond",          // Nom de la personne 
    prenom: "Jean",         // Prenom de la personne 
    age: 25,                // Age de la personne 
    sexe: "masculin",       // Sexe de la personne
    ville: "Armentières",   // Ville de la personne
    poids: "85",            // Poids de la personne
    taille: "1.80",         // Taille en mètre de la personne  

    decrire: function () {
        let description;    // Contient le message de description 
        description = "Le patient " +this['prenom'] + " " + this['nom'] + " de sexe " + this['sexe'] + " est agé de " + this['age'] +" ans"+". Il mesure " +this['taille']+ "m et pèse "+ this['poids']+ "kg";
        return description; // retourne le message de description 
    },

    definir_corpulence: function () {
        let message = "Votre imc est de : ";
        let poids = this.poids;
        let taille = this.taille;
        let imc;

         function Calculer_IMC() {
            imc = poids / (taille * taille); // Calcul IMC
            message += imc.toFixed(2)+" "; // Ajout de l'imc avec une limitation à 2 digits après la virgule
        }

        function interpreter_IMC() {

            if (this.imc < 16.5) {
                // Si l'imc est inférieur à 16.5 Alors
                message += "Vous etes en Dénutrition"; // MAJ de l'interpretation
            }
            if (this.imc > 16.5 && this.imc < 18.5) {
                // Si l'imc est supérieur à 16.5  et inférieur à 18.5 Alors
                message += "Vous etes en etat de maigreur"; // MAJ de l'interpretation
            }
            if (this.imc > 18.5 && this.imc < 25) {
                // Si l'imc est supérieur à 18.5 et inférieur à 25 Alors
                message += "Vous etes en etat de Corpulence normale"; // MAJ de l'interpretation
            }
            if (this.imc > 25 && this.imc < 30) {
                // Si l'imc est supérieur à 25  et inférieur à 30 Alors      
                message += "Vous etes en etat de surpoids"; // MAJ de l'interpretation
            }
            if (this.imc > 30 && this.imc < 35) {
                // Si l'imc est supérieur à 30 et inférieur à 35 Alors      
                message += "Vous etes en etat d'obesite modérée"; // MAJ de l'interpretation
            }
            if (this.imc > 35 && this.imc < 40) {
                // Si l'imc est supérieur à 35  et inférieur à 40 Alors      
                message += "Vous etes en etat d'obesite severe"; // MAJ de l'interpretation
            }
            if (this.imc > 40) {
                // Si l'imc est supérieur à 40  
                message += "Vous etes en etat d'obesite morbide"; // MAJ de l'interpretation

            }
        }
        Calculer_IMC();
        interpreter_IMC();
        return message;
    },
    
};

 
console.log(objPersonne.decrire());         // Affichage du resultat de la fonction decrire
console.log(objPersonne.definir_corpulence());    // Affichage du resultat de la fonction Calculer_IMC
