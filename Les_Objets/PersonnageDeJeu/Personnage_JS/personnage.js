// programme qui permet de creer des personnages de jeu
// Thomas ALEXANDRE 28/04/2021

function Personnage(prmNom, prmNiveau) {
    this.nom = prmNom;          // Nom de la personne 
    this.niveau = prmNiveau;    // Niveau du personnage 
}

Personnage.prototype.saluer = function(){
    return this.nom+" vous salue!"; // Message de salutation
}


function Guerrier(prmNom,prmNiveau,prmArme) {
    Personnage.call(this,prmNom, prmNiveau); //Permet d'utiliser les propriétées du constructeur personnage
    this.arme = prmArme;    // Arme du guerrier

    Guerrier.prototype.combattre = function () {
        return this.nom + " est un guerrier de niveau "+ this.niveau + " qui se bat avec " + this.arme; // message de combat 
    }

}

Guerrier.prototype = Object.create(Personnage.prototype); // Permet à l'objet guerrier d'utiliser les fonctions Personnage


function Magicien(prmNom,prmNiveau,prmPouvoir){
    Personnage.call(this,prmNom, prmNiveau); //Permet d'utiliser les propriétées du constructeur personnage
    this.pouvoir = prmPouvoir; // Pouvoir du magicien

}

Magicien.prototype = Object.create(Personnage.prototype); // Permet à l'objet Magicien d'utiliser les fonctions Personnage


Magicien.prototype.posseder = function(){
    return this.nom +" est un magicien de niveau "+ this.niveau + " qui possède le pourvoir de " + this.pouvoir; // message qui permet de savoir le pouvoir du magicien 
}

// Création de l'objet
let objArthur = new Guerrier("Arthur","3","epee");
// IHM
console.log(objArthur.saluer());
console.log(objArthur.combattre());
// Création de l'objet
let objMerlin = new Magicien("Merlin","2","prédire les batailles.");
// IHM
console.log(objMerlin.saluer());
console.log(objMerlin.posseder());