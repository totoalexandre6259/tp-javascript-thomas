    // Programme qui permet de calculer et d'interpreter l'imc en fonction du sexe
    // Thomas ALEXANDRE 27/04/2021
    
    // Définition de la fonction constructeur
    function Personne(prmNom,prmPrenom,prmAge,prmSexe,prmPoids,prmTaille) {
        this.nom = prmNom ;  // Nom de la personne 
        this.prenom = prmPrenom ; // Prenom de la personne 
        this.age = prmAge ; // Age de la personne 
        this.poids = prmPoids; // Poids de la personne 
        this.taille = prmTaille; // Taille de la personne 
        this.sexe = prmSexe // Sexe de la personne 

        this.decrire = function() {
            let description;    // Message de description
            let sexe = this.sexe; // Sexe prends la valeur de la propriété sexe

            if(sexe === "Masculin"){ // Si le sexe est masculin
            description = "Le patient " +this.prenom + " " + this.nom + " est agé de " + this.age + " ans " +"de sexe "+this.sexe + ". Il mesure " +this.taille +"m et pèse "+this.poids +"kg" ; 
            }
            else{ // Sinon
            description = "La patiente " +this.prenom + " " + this.nom + " est agée de " + this.age + " ans "+"de sexe "+this.sexe + ". Elle mesure " +this.taille +"m et pèse "+this.poids +"kg" ; 
            }

            return description; // retourne le message description 
        } ;


        this.definir_corpulence = function () {
            let message = "Son imc est de : "; // Message retourné
            let poids = this.poids; // poids prends la valeur de la propriété poids
            let taille = this.taille; // taille prends la valeur de la propriété taille
            let sexe = this.sexe; // Sexe prends la valeur de la propriété sexe
            let imc;    // Imc de la personne 
    
             function Calculer_IMC() {
                imc = poids / (taille * taille); // Calcul IMC
                if(sexe === "Masculin"){ // Si sexe = Masculin
                    imc = imc + 2;  // imc +2 car c'est un homme
                }
                message += imc.toFixed(2)+" "; // Ajout de l'imc avec une limitation à 2 digits après la virgule
            }
    
            function interpreter_IMC() {

            if(sexe === "Masculin"){    // Si le sexe est Masculin
                message += "Il "
            }
            else{ // Sinon
                message += "Elle "
            }
                if (imc < 16.5) {
                    // Si l'imc est inférieur à 16.5 Alors
                    message += "est en Dénutrition."; // MAJ de l'interpretation
                }
                if (imc > 16.5 && imc < 18.5) {
                    // Si l'imc est supérieur à 16.5  et inférieur à 18.5 Alors
                    message += "est en etat de maigreur."; // MAJ de l'interpretation
                }
                if (imc > 18.5 && imc < 25) {
                    // Si l'imc est supérieur à 18.5 et inférieur à 25 Alors
                    message += "est en etat de Corpulence normale."; // MAJ de l'interpretation
                }
                if (imc > 25 && imc < 30) {
                    // Si l'imc est supérieur à 25  et inférieur à 30 Alors      
                    message += "est en etat de surpoids."; // MAJ de l'interpretation
                }
                if (imc > 30 && imc < 35) {
                    // Si l'imc est supérieur à 30 et inférieur à 35 Alors      
                    message += "est en etat d'obesite modérée."; // MAJ de l'interpretation
                }
                if (imc > 35 && imc < 40) {
                    // Si l'imc est supérieur à 35  et inférieur à 40 Alors      
                    message += "est en etat d'obesite severe."; // MAJ de l'interpretation
                }
                if (imc > 40) {
                    // Si l'imc est supérieur à 40  
                    message += "est en etat d'obesite morbide."; // MAJ de l'interpretation
    
                }
            }

            Calculer_IMC(); // Appel de la fonction Calculer_IMC
            interpreter_IMC(); // Appel de la fonction Interpreter_IMC
            return message; // retourne le message 
        }

    }
 
    // création des objets
    let objPersonne1 = new Personne('Dupond', 'Jean', 30,'Masculin',85 ,1.80) ;
    let objPersonne2 = new Personne('Moulin', 'Isabelle',46, 'Feminin',90,1.65);
    let objPersonne3 = new Personne('Martin','Eric',42,'Masculin' ,90 ,1.70);


    // affichage de la description des objets
    console.log(objPersonne1.decrire()) ;
    console.log(objPersonne1.definir_corpulence()) ;
    console.log(objPersonne2.decrire()) ;
    console.log(objPersonne2.definir_corpulence()) ;
    console.log(objPersonne3.decrire()) ;
    console.log(objPersonne3.definir_corpulence()) ;

